# ICIR
These documents are part of the ImmunoCancer International Registry (ICIR) project and are only meant for use within this project.
This substudy has the objective to elucidate the impact of distinct immunosuppressive irAE management regimens on survival in a retrospective international big data network approach.

> **Note**
> Please use the Excel files with Macro's as instructed below using the _.zip_ download. If these doesn't work, use the Excel file without Macro's (_.xlsx_) and fill in 01-01-birthyear instead of the date of birth (e.g. 01-01-1950 instead of 21-04-1950) because of privacy.

## Download instructions
The template Excel sheets with macro's can be downloaded above and should be unzipped before use:
- Click on `ICIR_database_template_2022_04_01.zip` above (the file preceded by the greenish icon);
- Click on `Download (48.47 KiB)` in the middle of the screen to download the zipped files;
- One file can be found within the zipped file:
    - `ICIR_database_template_2022-06-23_empty.xlsm`: an empty data file that can be used to collect the data;
- Unzip and save this file in a directory of choice;
- Open the file.

> **Note**
> For previous versions of the template database or the study protocol, please contact Rik Verheijden.

## Instructions for use
To collect data per subject:
- Open the file `ICIR_database_template_2022-06-23_empty.xlsm` and save using another name if desired;
- Collect the data in column B (the most left white column);
    - Note: you can enter dates here, they will be transformed into time-differences later;
- If you are ready with filling in the data of this subject, press `new subject` in the upper left corner;
    - This will transform dates into time-intervals (e.g. date of birth into age) to ensure patients' privacy;
    - After transformation, data of this patient will be transfered to the right `Saved data` part, in which all data will be stored;
    - The `Data entry` part will be resetted to blank (including the data validation), you can start collecting data for a new patient here.
- After data collection is finished, save the file.
- Email the complete data file to Rik Verheijden and send a separate email message to check whether the data file has arived.

## Important notes
- After clicking `new subject` it is impossible to return to the previous state;
- Changes can be made in the `Saved data` part (right side) if necessary;
- Do not alter or delete the other tabs (`hidden_template` or `validation`);
- In case of questions, remarks or if Excel files with macro's are not supported in your institution, please contact Rik Verheijden.


# Contact information
Please contact Rik Verheijden for any questions regarding the project including the protocol: [R.J.Verheijden-6@umcutrecht.nl](mailto:R.J.Verheijden-6@umcutrecht.nl).

